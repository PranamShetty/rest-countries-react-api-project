import React from "react";
import { Routes, Route } from "react-router-dom";
import Header from "./component/Header";
// import Filter from "./component/Filter";
import Country from "./component/Country";
import Countries from "./component/Countries";
import "./App.css";

function App() {
  return (
    <>
      <Header />
      {/* <Filter/> */}
      <Routes>
        {/* <div className="flex-container">   */}
        <Route exact path="/" element={<Countries />} />
        {/* </div> */}

        <Route path="/countries/:name" element={<Country />}></Route>
      </Routes>
    </>
  );
}

export default App;
