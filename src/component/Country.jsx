import React from "react";
import { Link, useParams,Route } from "react-router-dom";
import { useState, useEffect } from "react";
import "../country.css";

const Country = () => {
  const [country, setCountry] = useState([]);
  const { name } = useParams();

  useEffect(() => {
    const fetchCountryData = async () => {
      const response = await fetch(
        `https://restcountries.com/v3.1/alpha/${name}`
      );
      const country = await response.json();
      setCountry(country);
      console.log(country);
    };

    fetchCountryData();
  }, [name]);

  return (
    <>
     <Link to="/" className="btn btn-light">
  &#8592; Back
</Link>

      <section className="country">
        {country.map((c) => {
          const {
            cca3,
            flags,
            name,
            population,
            region,
            subregion,
            capital,
            tld,
            currencies,
            languages,
            borders,
          } = c;

          let getRequiredData = (requiredObj) => {
            let val = [];
            let keys;
            if (requiredObj !== undefined) {
              keys = Object.keys(requiredObj);
            }
            if (keys !== undefined) {
              keys.map((key) => {
                let a = key;
                let check = requiredObj[a];
                check["name"]
                  ? val.push(requiredObj[a]["name"])
                  : val.push(requiredObj[a]);
              });
            } else {
              val.push("N.A.");
            }
            return val.join(",");
          };


          let getNativeName = (obj) => {
            if (obj !== undefined) {
              let keys = Object.keys(obj);
              return obj[keys[0]].common;
            } else {
              return "NA";
            }
          };
        
        

          return (
            <article className = "country-flex" key={cca3}>
              <div className="flag">
                <img class = "country-image" src={flags.png} alt={name} />
              </div>
              <div>


              <div className="country-details">

                <h2 className="CountryName">{name.common}</h2>

                <div className="country-details-main">


                <div className="country-details-1">
                  <h5>Native Name: {getNativeName(name.nativeName)} </h5>
                  <h5>
                    Population: <span> {population} </span>{" "}
                  </h5>
                  <h5>
                    Region: <span> {region}</span>
                  </h5>
                  <h5>
                    Sub Region: <span>{subregion}</span>{" "}
                  </h5>
                  <h5>
                    Capital: <span>{capital}</span>{" "}
                  </h5>
                </div>

                <div className="country-details-2">
                  <h5>
                    Top Level Domain: <span>{getRequiredData(tld)}</span>{" "}
                  </h5>
                  <h5>
                    Currencies: <span>{getRequiredData(currencies)}</span>
                  </h5>
                  <h5>
                    Languages:<span>{getRequiredData(languages)}</span>
                  </h5>
                </div>

                </div>


              </div>



              <div>

      
                <div className="border-contries">
                  <p>Border countries: </p>
                  <div className="borders">
                    {borders &&
                      borders.map((t) => (
                        <Link to={`../countries/${t}`} key={t}>
                          <button className="each-country"
                          >
                            {t}
                          </button>
                        </Link>
                      ))}
                  </div>
                </div>



              </div>

              </div>
            </article>
          );
        })}
      </section>
    </>
  );
};

export default Country;
