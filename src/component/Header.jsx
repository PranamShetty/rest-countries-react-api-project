import React from "react";

const Header = () => {
  return (
    <>
      <header className="header">
        <div>
          <h1> Where in the world?</h1>
        </div>

        <div>
        <span class="fas fa-moon">&#9790; Dark Mode</span>
        </div>
      </header>
    </>
  );
};

export default Header;
