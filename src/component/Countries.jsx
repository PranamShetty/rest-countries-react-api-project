import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const Countries = () => {
  const [countries, setCountries] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("all");

  const url = "https://restcountries.com/v3.1/all";

  const fetchCountryData = async () => {
    try {
      const response = await axios.get(url);
      const countries = response.data;
      setCountries(countries);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchCountryData();
  }, []);

  const handleSearchInputChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const handleRegionSelectChange = (event) => {
    setSelectedRegion(event.target.value);
  };

  const filteredCountries = countries.filter((country) => {
    // Filter by search query
    const name = country.name.common.toLowerCase();
    const searchQueryLower = searchQuery.toLowerCase();
    if (!name.startsWith(searchQueryLower)) {
      return false;
    }

    // Filter by selected region
    if (selectedRegion !== "all" && country.region !== selectedRegion) {
      return false;
    }

    return true;
  });

  return (
    <>
      <div className="filter-container">
        <input
        className="take-input"
          type="text"

          placeholder="&#xf002; Search country..."

          value={searchQuery}
          onChange={handleSearchInputChange}
        />
        <select value={selectedRegion} onChange={handleRegionSelectChange}>
          <option value="all">All Regions</option>
          <option value="Africa">Africa</option>
          <option value="Americas">Americas</option>
          <option value="Asia">Asia</option>
          <option value="Europe">Europe</option>
          <option value="Oceania">Oceania</option>
        </select>
      </div>

      <div className="flex-container">
        {filteredCountries.map((country) => {
          const { cca3, name, population, region, capital, flags } = country;

          return (
            <div className = "countryContainer" key={cca3}>
                <Link className="LinkContainer" to={`../countries/${cca3}`} key={cca3}>
                  <img className = "countriesImg" src={flags.png} alt="" />
                  <div className="country-details">
                    <h2>{name.common}</h2>
                    <h4>
                      Population: <span>{population}</span>
                    </h4>
                    <h4>
                      Region: <span>{region}</span>
                    </h4>
                    <h4>
                      Capital: <span>{capital ? capital : "N/A"}</span>
                    </h4>
                  </div>
                </Link>
            
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Countries;
